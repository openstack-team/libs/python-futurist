python-futurist (3.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * d/watch: switch to version=4 and mode=git.

 -- Thomas Goirand <zigo@debian.org>  Tue, 25 Feb 2025 16:51:01 +0100

python-futurist (3.0.0-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090504).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 10:48:19 +0100

python-futurist (3.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Apr 2024 16:14:49 +0200

python-futurist (3.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Feb 2024 13:35:44 +0100

python-futurist (2.4.1-3) unstable; urgency=medium

  * Cleans better (Closes: #1047361).

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Aug 2023 11:45:01 +0200

python-futurist (2.4.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 11:55:17 +0200

python-futurist (2.4.1-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-six from (build-)depends.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Aug 2022 11:42:00 +0200

python-futurist (2.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 10:20:09 +0200

python-futurist (2.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Sep 2021 10:43:28 +0200

python-futurist (2.3.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add a debian/salsa-ci.yml.
  * Add python3-greenlet as (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Oct 2020 12:32:09 +0200

python-futurist (2.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 11:00:34 +0200

python-futurist (2.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 Sep 2020 14:34:58 +0200

python-futurist (2.1.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 10:26:54 +0200

python-futurist (2.1.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Apr 2020 16:31:08 +0200

python-futurist (2.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Remove python3-prettytable from runtime depends.

 -- Thomas Goirand <zigo@debian.org>  Tue, 31 Mar 2020 14:18:45 +0200

python-futurist (1.9.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 20 Oct 2019 19:14:19 +0200

python-futurist (1.9.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2019 22:08:15 +0200

python-futurist (1.8.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 16 Jul 2019 21:13:50 +0200

python-futurist (1.8.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Removed Python 2 support.
  * Using python3 -m sphinx to build the doc.

 -- Thomas Goirand <zigo@debian.org>  Wed, 27 Mar 2019 11:43:10 +0100

python-futurist (1.6.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 22:56:30 +0000

python-futurist (1.6.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Bumped debhelper compat version to 10

  [ Thomas Goirand ]
  * d/rules: Changed UPSTREAM_GIT protocol to https.
  * Points VCS URLs to Salsa.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Standards-Version is now 4.1.3.
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using pkgos-dh_auto_{install,test}.

 -- Thomas Goirand <zigo@debian.org>  Sun, 11 Feb 2018 10:55:53 +0000

python-futurist (0.13.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/s/options: extend-diff-ignore of .gitreview
  * d/control: Using OpenStack's Gerrit as VCS URLs.

  [ Thomas Goirand ]
  * Fixed oslotest EPOCH.

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

 -- Ondřej Nový <novy@ondrej.org>  Sat, 09 Apr 2016 19:26:50 +0200

python-futurist (0.13.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 09:25:07 +0000

python-futurist (0.13.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version: 3.9.7 (no change).
  * Disabled futurist.tests.test_executors.TestRejection.test_rejection which
    is failing.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Mar 2016 13:06:31 +0000

python-futurist (0.9.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Sat, 16 Jan 2016 03:48:28 +0000

python-futurist (0.5.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2015 09:31:19 +0000

python-futurist (0.5.0-1) experimental; urgency=medium

  [ James Page ]
  * d/rules: Drop use of dpkg-parsechangelog -S to ease backporting to
    older Debian and Ubuntu releases.

  [ Corey Bryant ]
  * New upstream release.
  * Align dependencies with upstream.

  [ Thomas Goirand ]
  * Drop versions already satisfied in Trusty.

 -- Thomas Goirand <zigo@debian.org>  Mon, 05 Oct 2015 08:21:43 +0000

python-futurist (0.1.2-1) unstable; urgency=medium

  * Initial release. (Closes: #792500)

 -- Thomas Goirand <zigo@debian.org>  Wed, 15 Jul 2015 14:47:49 +0200
